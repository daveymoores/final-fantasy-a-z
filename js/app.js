// Refactoring to use OOP and try and make this all a little less expensive

$(document).foundation();



$(document).ready(function() {
    react_to_window();
});

//      only activate stellar for window widths above or equal to 1024
var stellarActivated = false;

$(window).resize(function() {
    react_to_window();
});

function react_to_window() {
    if ($(window).width() <= 1024) {
        if (stellarActivated == true) {
            $(window).data('plugin_stellar').destroy();
            stellarActivated = false;
        }
    } else {
        if (stellarActivated == false) {

             $.stellar({
			  	positionProperty: 'transform',
			  	horizontalOffset: 0
			  });

            $(window).data('plugin_stellar').init();
            stellarActivated = true;
        }
    }
}



$(document).ready(function(){






//--------------------all alphabet stuff------------------------//

	var waypoint2 = new Waypoint({
	  element: $('#letter_content'),
	  handler: function(direction) {

	  	if(direction == 'down') {

	  		$('#alphabet_hidden-menu').velocity({
	  			top : 0
	  		},'easeOutSine', 200);

	  		$('#alphList').find('li').each(function(p){

		  		var time = 30*p;
		  		var $ele = $(this);

		  		setTimeout(function(){

		  			$ele.velocity({
		  				marginTop: '20px'
		  			},'easeOutSine', 200);

		  		}, time);


		  });

	  	} else {


	  		$($('#alphList').find('li').get().reverse()).each(function(p){

		  		var time = 20*p;
		  		var $ele = $(this);

		  		setTimeout(function(){

		  			$ele.velocity({
		  				marginTop: '-25px'
		  			},'easeInSine', 200);

		  		}, time);

		  		if(p == 25) {

		  			$('#alphabet_hidden-menu').velocity({
		  				top : '-60px'
		  			},'easeOutSine', 200);
		  		}

		    });

	  	}



	  },
	  offset: '0%'
	});



	$('#alphList').stop().on('mouseenter', 'li', function(){

		$(this).velocity({
			marginTop: '25px'
		},'easeInSine', 200);

	});


	$('#alphList').stop().on('mouseleave', 'li', function(){

		$(this).velocity({
			marginTop: '20px'
		},'easeInSine', 200);

	});

	$('#alphList').on('click', 'a', function(){

			var charNu = $(this).text().toLowerCase(),
				destination = $('#ff_wrapper').find("[data-letter='" + charNu + "']");

			$.scrollTo( destination, 800, {easing:'swing'});

	});


//---------------------end alphabet stuff---------------------------//


  $(".lazy").lazyload({
      effect : "fadeIn"
  });

	//set links and scrollto
	$('#ff_wrapper').on('click', 'a', function(){

			var charNu = $(this).text().toLowerCase(),
				destination = $('#ff_wrapper').find("[data-letter='" + charNu + "']");

			$.scrollTo( destination, 800, {easing:'swing'});

	});
	//end scrollTo




	$(".bg").interactive_bg({
	   strength: 40,
	   scale: 1,
	   animationSpeed: "100ms",
	   contain: true,
	   wrapContent: false
	 });

	  $(window).resize(function() {
	      $(".bg > .ibg-bg").css({
	        width: $(window).outerWidth(),
	        height: $(window).outerHeight()
	      })
	   });

	//create a lazyload function and then initialise plugins on the fly as they enter the page
	var $el = $('.ff_image'),
		$window = $(window),
		unitHeight = $el.outerHeight(),
		unitWidth = $el.outerWidth();


	// Async so that the sizes of the images are calculated properly on lazyload
	var addingSizes = new $.Deferred();

	addingSizes.done(function(input) {
	  input.each(function(){
	  	$(this).css('height', unitHeight).css('width', unitWidth);
	  });
	});


	addingSizes.resolve($el);

	addingSizes.done(function() {
	  $(".ff_image").lazyload({
	      effect : "fadeIn"
	  });
	});



	/**-----------------getting the panels to fit-----------------**/

	var winWidth = $window.width(),
		percentageWidth = winWidth / 100,
		newUnitWidth = Math.floor(percentageWidth*65);


	if(winWidth < 1024 && winWidth > 700) {
		newUnitWidth = percentageWidth*55;
	}


	if(winWidth < 700) {
		newUnitWidth = percentageWidth*100;
	}


	$el.css('width', newUnitWidth);

	//set new width inline on window resize
	$window.on('resize', Foundation.utils.throttle(function(e){

		var winWidth = $window.width(),
			percentageWidth = winWidth / 100,
			newUnitWidth = Math.floor(percentageWidth*65);


		if(winWidth < 1024 && winWidth > 700) {
			newUnitWidth = percentageWidth*55;
		}


		if(winWidth < 700) {
			newUnitWidth = percentageWidth*100;
		}


    	$el.css('width', newUnitWidth);

	}, 300));


	/**-----------------end getting the panels to fit-----------------**/



	/**tiny ball animation over letters**/

	//get offset from each letter to edge of browser window
	//get width of letter/2 add to above
	//set balls left attribute to that number

	var $ballWrapperOne = $('#ball_container_1'),
		$ballWrapperTwo = $('#ball_container_2'),
		$ballOne = $('#ball_1'),
		$ballTwo = $('#ball_2');


	function moveBall(el, container, ball){

			var w_alpha = (el.outerWidth())/2,				//middle of li
			ulOffset = container.offset().left,				//distance from container to window left
			w_win = el.offset().left - ulOffset,			//distance from li - distance from ul
			ballPos = w_alpha+w_win;						//middle of li + distance inside ul


			if(container.hasClass('firstenter') == true) {

				var snapTo = new $.Deferred();


				snapTo.done(function(input){

					input.css('left', ballPos);

				});

				snapTo.resolve(ball);


				snapTo.done(function(){

					ball.stop().animate({
						opacity: 1
					}, 250, function(){
						container.removeClass('firstenter');
					});


				});


			} else {

				ball.stop().velocity({
					left : ballPos,
					opacity: 1
				},'swing', 200);

			}

	}



	//mouse moving between letters
	$ballWrapperOne.on('mouseenter', 'li', function(){

		var $this = $(this);
		$this.addClass('lit');
		moveBall($this, $ballWrapperOne, $ballOne);

	});

	$ballWrapperOne.on('mouseleave', 'li', function(){

		var $this = $(this);
		$this.removeClass('lit');

	});

	//mouse entering wrapper
	$ballWrapperOne.on('mouseenter', function(){

		$(this).addClass('firstenter');

	});


	//mouseleaving wrapper
	$ballWrapperOne.on('mouseleave', function(){

			$ballOne.stop().velocity({
				opacity :0
			}, 250);

	});


	//mouse moving between letters - row 2
	$ballWrapperTwo.on('mouseenter', 'li', function(){

		var $this = $(this);
		$this.addClass('lit');
		moveBall($this, $ballWrapperTwo, $ballTwo);

	});


	$ballWrapperTwo.on('mouseleave', 'li', function(){

		var $this = $(this);
		$this.removeClass('lit');

	});

	//mouse entering wrapper - row 2
	$ballWrapperTwo.on('mouseenter', function(){

		$(this).addClass('firstenter');

	});

	//mouseleaving wrapper - row 3
	$ballWrapperTwo.on('mouseleave', function(){

			$ballTwo.stop().velocity({
				opacity :0
			}, 250);

	});



	/**end tiny ball animation**/




	//FRAMERATE SHIZ
	/************************/
	/************************/

	var movers      = $el,
    lastScrollY     = 0,
    ticking         = false;


	/**
	 * Callback for our scroll event - just
	 * keeps track of the last scroll value
	 */
	function onScroll() {
	    lastScrollY = window.scrollY;
	    requestTick();
	}

	/**
	 * Calls rAF if it's not already
	 * been done already
	 */
	function requestTick() {
	    if(!ticking) {
	        requestAnimationFrame(update);
	        ticking = true;
	    }
	}

	/**
	 * Our animation callback
	 */
	function update() {
	    var mover               = null,
	        moverTop            = [],
	        offset              = 0;

		// first loop is going to do all
		// the reflows (since we use offsetTop)
	    for(var m = 0; m < movers.length; m++) {

	        mover       = movers[m];
	        moverTop[m] = $el.eq(m-1).offsetTop;

	        //console.log(moverTop[m]);

	    }

		// second loop is going to go through
		// the movers and add the left class
		// to the elements' classlist
	    for(var m = 0; m < movers.length; m++) {

	        mover       = movers[m];

	        if(m == 0) {
            	var yPos = -($window.scrollTop() / $(mover).data('speed'));
            } else if(m>0) {
            	var elTop = $el.eq(m-1).offset().top + $(mover).height(),
            		distance = ($window.scrollTop() - elTop),
            	    yPos = -(distance / $(mover).data('speed')) - 120;

            }

            // Put together our final background position
            var coords = '90% '+ yPos + 'px';

            // Move the background
            $(mover).css({ backgroundPosition: coords });

	    }

		// allow further rAFs to be called
	    ticking = false;
	}

	//only listen for scroll events
	//window.addEventListener('scroll', onScroll, false);



	/************************/
	/************************/
	//END OF FRAMERATE

	var divs = $('#textWrapper, .ibg-bg');
	$(window).scroll(function(){
	    var top = (($(window).scrollTop() > 0) ? $(window).scrollTop() : 1)/100;
	    console.log(top);
	    divs.stop(true, true).fadeTo(0, 1 / top);
	});


  //parallax function
  function parallaxInit(element, image, window, i){

  	console.log(element);

  		//reveal text
  		element.find('.sub').each(function(e){

  			if ($(this).hasClass('visible') !== true) {

	  			var $sub = $(this),
	  				timeoutInterval = e*500;

	  			setTimeout(function() {

		  			if($sub.parents('.ff_article').hasClass('reverse') !== true) {

						$sub.velocity({
				  			marginRight: 0,
				  			opacity: 1,
				  		}, 'easeOutSine', 800).addClass('visible');

		  			} else {

		  				$sub.velocity({
				  			marginLeft: 0,
				  			opacity: 1,
				  		}, 'easeOutSine', 800).addClass('visible');

		  			}


	  			}, timeoutInterval);

  			}

  		});


	  }


	$('#ff_wrapper').find('.ff_article').each(function(i){

		var $obj = $(this).find('.ff_title');

		var waypoint = new Waypoint({
		  element: $(this),
		  handler: function(direction) {

		    parallaxInit($obj, $el, $window, i);

		  },
		  offset: '75%'
		});


	});



	$('.ff_image[data-type="background"]').each(function(i){
        var $bgobj = $(this); // assigning the object
        var bool = false;
        var unitHeight = ($(this).height()*i);

        //parallaxInit($bgobj, $el, $window, i); //initialise on page load
        //onScroll();

    });



/**share btn**/

new Share(".share_btn", {
		ui: {
		  flyout: "bottom center"
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    },
		    facebook: {
		      enabled: true,
		      load_sdk: true,// Load the FB SDK. If false, it will default to Facebook's sharer.php implementation.
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "http://uk-microsites.ign.com/thewitcher3/",
		      //app_id: "551643344962507",
		      title: "Vote for your favourite monster movie with IGN and The Witcher 3",
		      caption: "Vote for your favourite monster movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    },
		    email: {
		      enabled: true,
		      title:     "Vote for your favourite monster movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    }
		  }
});

/**end share btn**/


});
